(function($,Materialize,CryptoJS){
  angular.module('geaControllers', ['ngRoute','ngLocale','ngSanitize','ngAria','validation.match'])
  .run(function($http) {
    $http.defaults.headers.common.From = 'gea-webClient';
  })
  .controller('appController',['$rootScope',"session",'shoppingCart',function(scope,session,shoppingCart){
    session.update();
    // shoppingCart
  }])
  .controller('signController',['$routeParams','$location','$scope','$http','session',function(params,location,scope,http,session){
    if(session.valid()){
      location.path('/');
    }
    else{
      var form = {
        email:"",
        password:"",
        confirm:{
          email:"",
          password:""
        }
      }
      this.form=form;
      scope.path='/';
      scope.host='http://'+location.host();
      console.log(scope.host);
      Materialize.updateTextFields();
      switch (params.action) {
        case 'in':
        scope.new=false;
        scope.path='/sign'
        break;
        case 'up':
        scope.new=true;
        scope.path='/cuenta'
        break;
        default:
        scope.new=false;
        location.path('/404');
      }
      this.reset=function(form){
        form.$setPristine();
        this.form=form;
      }
      this.submit=function(){
        var req = {
         method: 'POST',
         url:scope.host+':19100'+scope.path,
        //  headers: {
        //    'User-Agent': navigator.userAgent,
        //  },
         data:{email:this.form.email,password:this.form.password}
        }
        http(req)
        .then(function(res){
          console.log(res.data);
          session.open(res.data.body.jwt);
        },function(res){
          alert(res.data.body.message);
          session.update();
        });
      }
      scope.title=params.action;
    }
  }])
  .controller('homeController',['$routeParams','$rootScope','session','$location','$http','shoppingCart',function(params,scope,session,location,http,shoppingCart){
    if(session.valid()){
      // scope.searchBox='';
      scope.host='http://'+location.host();
      http.get(scope.host+':19100/productos')
      .then(function(res){
        scope.list=res.data.body;
      });
    }
    else{
      location.path('/sign/in');
    }
  }])
  .controller('infoController',['$routeParams','$scope','session','$location',function(params,scope,session,location){
    if(session.valid()){

    }
    else{
      location.path('/sign/in');
    }
  }])
  .controller('panelController',['$routeParams','$scope','session','$location','$http',function(params,scope,session,location,http){
    if(session.valid()){
      scope.transacciones=[];
      scope.productos=[];
      // scope.ventas=[];
      http.get('http://'+location.host()+':19100/transacciones/usuario/'+session.id())
      .then(function(res){
        // console.log(res.data);
        scope.transacciones=res.data.body;
      })
      http.get('http://'+location.host()+':19100/productos/ventas/'+session.id())
      .then(function(res){
        // console.log(res.data);
        scope.productos=res.data.body;
        // Grafica de Pastel
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
          var arreglo = [
            ['Producto', 'Ventas']
          ];
          for (var producto of scope.productos) {
            arreglo.push([producto.nombre,producto._data.length || 0])
          };
          var data = google.visualization.arrayToDataTable(arreglo);

          var options = {
            title: 'Mis Ventas'
          };

          var chart = new google.visualization.PieChart(document.getElementById('piechart'));

          chart.draw(data, options);
        }
      })
      // http.get('http://'+location.host()+':19100/transacciones/vendedor/'+session.id())
      // .then(function(res){
      //   // console.log(res.data);
      //   scope.ventas=res.data.body;
      //   console.log(scope.ventas);
      // })

      // ---------------------------------------------
      scope.ventaActual={};
      scope.contactoVentas=function(index){
        scope.ventaActual=scope.productos[index];
        //crear Modal para ver detalle de la venta
      }
    }
    else{
      location.path('/sign/in');
    }
  }])
  .controller('ubicController',['$routeParams','$scope','session','$location',function(params,scope,session,location){
    if(session.valid()){

    }
    else{
      location.path('/sign/in');
    }
  }])
  .controller('prodController',['$routeParams','$scope','session','$location','$http',function(params,scope,session,location,http){
    if(session.valid()){
      var form ={
        nombre:'',
        descripcion:'',
        imagen:'',
        presentacion:[
          {
            unidad:'',
            precio:''
          }
        ]
      }
      this.form = form;
      scope.path='/producto';
      scope.host='http://'+location.host();
      this.submit=function(){
        this.form.usuario=session.id();
        delete this.form['$$hashKey'];
        for (var presentacion in this.form.presentacion) {
          delete presentacion['$$hashKey'];
        }
        console.log(this.form);
        http.post(scope.host+':19100'+scope.path,this.form)
        .then(function(res){
          console.log(res.data);
        });
        location.path('/');
      };
      this.reset=function(form){
        form.$setPristine();
        this.form = {
          nombre:'',
          descripcion:'',
          imagen:'',
          presentacion:[
            {
              unidad:'',
              precio:''
            }
          ]
        };
      };
      this.addSubElement=function(){
        this.form.presentacion.push({
          unidad:'',
          precio:''
        });
        console.log(this.form);
      }
    }
    else{
      location.path('/sign/in');
    }
  }])
  .factory('session',['$rootScope','$location','$route','$routeParams','$timeout',function(scope,location,route,params,timeout){
    var base64url={
      encode:function(string){
        return CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(string));
      },
      decode:function(string){
        return CryptoJS.enc.Base64.parse(string).toString(CryptoJS.enc.Utf8);
      }
    };
    scope.user={
      id:'',
      email:'',
      exp:'',
      jti:'',
      free:true
    }
    scope.session={
      valid:function(){
        return !((localStorage.user===undefined || localStorage.user==='') || scope.user.free || Number(scope.user.exp)<Date.now());
      },
      update:function(){
        if((!this.valid() && (localStorage.user && localStorage.user===scope.user.id && localStorage.user!=''))){
          this.close();
        };
        scope.user={
          id:localStorage.user,
          email:localStorage.email,
          exp:localStorage.exp,
          jti:localStorage.jti,
          free:localStorage.jwt===''
        };
        timeout(function(){
          $('.tooltipped').tooltip({delay: 50});
        },250)
      },
      close:function(){
        alert("Sesión finalizada");
        localStorage.setItem('user','');
        localStorage.setItem('email','');
        localStorage.setItem('exp','');
        localStorage.setItem('jti','');
        localStorage.setItem('jwt','');
        this.update();
        if(location.path()==='/'){
          route.reload();
        }
        else{
          location.path('/');
        }
      },
      open:function(jwt){
        var token = JSON.parse(base64url.decode(jwt.split('.')[1]));
        localStorage.setItem('user',token.user);
        localStorage.setItem('email',token.email);
        localStorage.setItem('exp',token.exp);
        localStorage.setItem('jti',token.jti);
        localStorage.setItem('jwt',jwt);
        this.update();
        location.path('/');
      },
      token:function(){
        return 'Bearer '+localStorage.jwt;
      },
      id:function(){
        return scope.user.id;
      }
    }
    scope.session.valid();
    return scope.session;
  }])
  .factory('shoppingCart',['$rootScope','$timeout','$http',function(scope,timeout,http){
    $('select').material_select();
    return scope.shoppingCart={
      open:function(){
        $('#ShoppingCart').openModal();
      },
      close:function(){
        $('#ShoppingCart').closeModal();
      },
      product:{
        add:function(product){
          $('#ShoppingCart').openModal();
          scope.shoppingCart.tmp.push(product);
          timeout(function(){
            $('ul.tabs').tabs();
            $('ul.tabs').tabs('select_tab', 'shoppingForm');
          },100)
          // console.log(scope.shoppingCart.tmp);
        },
        confirm:function(product,index){
          // console.log(product);
          var prod = {
            producto:product._id,
            usuario:product.usuario,
            presentacion:product.presentacion[product.selected]._id,
            cantidad:product.cantidad,
            subtotal:product.subtotal,
            _data:product
          }
          scope.shoppingCart.form.total+=product.subtotal;
          scope.shoppingCart.form.productos.push(prod);
          // console.log(scope.shoppingCart.form.productos);
          scope.shoppingCart.tmp.splice(index,1);
          timeout(function(){
            $('ul.tabs').tabs();
            $('ul.tabs').tabs('select_tab', 'verCarrito');
          },100)
        },
        remove:function(index){
          scope.shoppingCart.form.total -= scope.shoppingCart.form.productos[index].subtotal;
          scope.shoppingCart.form.productos.splice(index,1);
          timeout(function(){
            $('ul.tabs').tabs();
            if(scope.shoppingCart.form.productos.length===0 && scope.shoppingCart.tmp.length>0){
              $('ul.tabs').tabs('select_tab', 'shoppingForm');
            }
            if(scope.shoppingCart.form.productos.length===0 && scope.shoppingCart.tmp.length===0){
              $('#ShoppingCart').closeModal();
            }
          },100)
        },
        cancel:function(index){
          scope.shoppingCart.tmp.splice(index,1);
          timeout(function(){
            $('ul.tabs').tabs();
            if(scope.shoppingCart.tmp.length===0 && scope.shoppingCart.form.productos.length>0){
              $('ul.tabs').tabs('select_tab', 'verCarrito');
            }
            if(scope.shoppingCart.tmp.length===0 && scope.shoppingCart.form.productos.length===0){
              $('#ShoppingCart').closeModal();
            }
          },100)
        }
      },
      tmp:[],
      form:{
        productos:[],
        total:0
      },
      submit:function(){
        delete this.form['$$hashKey'];
        for (var productos in this.form.productos) {
          delete productos['$$hashKey'];
          delete productos['_data'];
        };
        this.form.usuario=scope.user.id;
        var req = {
         method: 'POST',
         url:scope.host+':19100/transaccion',
         data:this.form
        }
        http(req)
        .then(function(res){
          console.log(res.data);
          scope.shoppingCart.form={
            productos:[],
            total:0
          };
          $('#ShoppingCart').closeModal();
          alert("Su compra esta en proceso");
        },function(res){
          alert("Error, por favor intentelo más tarde");
          $('#ShoppingCart').closeModal();
        });
      },
      reset:function(form){
        form.$setPristine();
        this.form={
          productos:[],
          total:0
        }
      }
    }
  }])
  .factory('Materialize',function(){
    Materialize.init={
      select:function(){
        $('select').material_select();
      },
      date:function(obj){
        obj=obj||{
          selectMonths: true, // Creates a dropdown to control month
          selectYears: 99 // Creates a dropdown of 99 years to control year
        }
        $('.datepicker').pickadate(obj)
      },
      counterTo:function(element){
        element=element||'input:text'
        $(element).characterCounter();
      },
      modal:function(modal){
        $(modal).openModal();
      },
      tab:function(tab){
        $(tab).tabs();
      },
      parallax:function(parallax){
        $(parallax).parallax();
      }
    };
    Materialize.kill={
      select:function(){
        $('select').material_select('destroy')
      },
      modal:function(modal){
        $(modal).closeModal();
      }
    };
    Materialize.updateTextFields = Materialize.updateTextFields || function() {
      var input_selector = 'input[type=text], input[type=password], input[type=email], input[type=url], input[type=tel], input[type=number], input[type=search], textarea';
      $(input_selector).each(function(index, element) {
        if ($(element).val().length > 0 || element.autofocus ||$(this).attr('placeholder') !== undefined || $(element)[0].validity.badInput === true) {
          $(this).siblings('label, i').addClass('active');
        }
        else {
          $(this).siblings('label, i').removeClass('active');
        }
      });
    };

    var input_selector = 'input[type=text], input[type=password], input[type=email], input[type=url], input[type=tel], input[type=number], input[type=search], textarea';

    $(document).on('change', input_selector, function () {
      if($(this).val().length !== 0 || $(this).attr('placeholder') !== undefined) {
        $(this).siblings('label').addClass('active');
      }
      window.validate_field($(this));
    });
    return Materialize;
  });
})(jQuery,window.Materialize,CryptoJS);
