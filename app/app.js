(function($){
  angular.module('gea', ['ngRoute','ngAnimate','geaControllers'])
    .config(['$routeProvider',function(router) {
      router.when('/', {
        templateUrl: 'app/views/catalog.html',
        controller: 'homeController',
        controllerAs: 'home'
      })
      .when('/sign/:action', {
        templateUrl: 'app/views/sign.html',
        controller: 'signController',
        controllerAs: 'sign'
      })
      .when('/usuario/:id_usuario', {
        templateUrl: 'app/views/info.html',
        controller: 'infoController',
        controllerAs: 'info'
      })
      .when('/panel', {
        templateUrl: 'app/views/panel.html',
        controller: 'panelController',
        controllerAs: 'panel'
      })
      .when('/usuario/:id_usuario/ubicacion/:action/:id_ubication?', {
        templateUrl: 'app/views/ubic.html',
        controller: 'ubicController',
        controllerAs: 'ubication'
      })
      .when('/producto/:action/:id_producto?', {
        templateUrl: 'app/views/prod.html',
        controller: 'prodController',
        controllerAs: 'product'
      })
      .when('/404',{
        templeteUrl: 'app/views/404.html'
      }).otherwise({
        redirectTo: '/404'
      });
    }]);
})(jQuery);
